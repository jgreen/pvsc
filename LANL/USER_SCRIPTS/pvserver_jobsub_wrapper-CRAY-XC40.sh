#!/bin/bash
# ParaView Server job submission wrapper script. by 
# Maintainers: G.Cone @ LANL HPC-3, Chuck Atkins @ Kitware
#   Based on NERSC's PV server job submission wrapper script. Modified
#   for our systems.

usage(){
  cat<<EOF
    Usage: $0 NNODES NPROCS_PER_NODE HOURS SSH_PORT ACCOUNT QUEUE PV_VERSION
   
      NNODES          - number of compute nodes to use.
      NPROCS_PER_NODE - number of processes per compute node.
      HOURS           - number of hours for the job
      SSH_PORT        - port number used by SSH reverse tunnel
      ACCOUNT         - account to submit the job under
      QUEUE           - queue to submit the job under
      PV_VERSION      - ParaView version
EOF
} #END function usage

# if not enough arguments, then epic fail
if [ $# != 7 ]; then
  usage
  exit 1
fi

# Set up traps to kill any child processes
trap "echo Cleaning up... && trap - SIGTERM && kill -- -$$ 1>/dev/null 2>/dev/null" SIGINT SIGTERM EXIT

NNODES=$1
NPROCS_PER_NODE=$2
MINUTES=$(($3 * 60))
SSH_PORT=$4
ACCOUNT=$5
QUEUE=$6
PV_VERSION=$7

TOT_TASKS=$((NNODES*NPROCS_PER_NODE))

WALLTIME=${MINUTES}:00

# if the ACCOUNT variable is NOT "DEFAULT", add in an msub argument to 
# specify the account to use
if [ "$ACCOUNT" != "DEFAULT" ]; then
  ACCT_ARG="-A $ACCOUNT"
fi

# if the QUEUE variable is NOT "DEFAULT", add in an msub argument to 
# specify the queue to use
if [ "$QUEUE" != "DEFAULT" ]; then
  Q_ARG="-q $QUEUE"
fi

# Ensure the User is able to see the DOE "Notice to Users" banner.
cat /etc/motd
sleep 5

# Network interface netween the mom node and the nid
FE_IF=eth4
echo -n "Getting Front End IP for interface ${FE_IF}: "
FE_IP=$(ip addr show dev ${FE_IF} | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
echo ${FE_IP}

# Keep generating a random port number until we find one not in use
echo -n "Generating Front End Port Number: "
FE_PORT=$(($RANDOM+1024))
while netstat -ltnT | sed -n "s/^tcp \+[0-9]\+ \+[0-9]\+ \+\(0.0.0.0\|$FE_IP\):\([0-9]\+\).*/\1 \2/p" | grep $FE_PORT > /dev/null
do
  FE_PORT=$(($RANDOM+1024))
done
echo $FE_PORT

echo "Launching SOCAT tunnel between front end and SSH"
socat -d -d -lf socat-fe.log tcp4-listen:$FE_PORT,bind=$FE_IP,close tcp4-connect:localhost:$SSH_PORT &
TUNNEL_PID=$!

# Submit the PVserver job using an msub with a HereDoc:
echo "Submitting pvserver job for $TOT_TASKS tasks across $NNODES nodes for ${WALLTIME}"

MOM_IF=ipogif0
export FE_IP FE_PORT MOM_IF MINUTES PV_VERSION
PBS_SCRIPT=$(dirname $(readlink -f ${BASH_SOURCE}))/$(basename ${BASH_SOURCE} .sh).pbs
JOBID=$(msub $ACCT_ARG $Q_ARG -l walltime="${WALLTIME}" -l nodes=$NNODES:ppn=$NPROCS_PER_NODE -v FE_IP,FE_PORT,MOM_IF,MINUTES,PV_VERSION ${PBS_SCRIPT})
echo "JOBID: $(echo $JOBID)"

# Perform a do nothing while loop to periodically check every 5 seconds if the job is running:
echo "Waiting for job to enter the RUNNING state. Please be patient if system is busy."
echo "PLEASE DO NOT KILL THIS XTERM WINDOW!"

while ! checkjob $JOBID 2>&1 | grep "State: Running" > /dev/null;do
  sleep 5s
done

echo "pvserver allocation granted. Initating reverse connection between pvserver and your ParaView client."
sleep 10s
echo "Your ParaView client should now be connected to the pvserver."

# Now do another while loop to ensure that the job is still running (check every 5s).
while checkjob $JOBID | grep "State: Running" > /dev/null;do
  sleep 5s
done

kill -9 ${TUNNEL_PID} 1>/dev/null 2>/dev/null

echo "Job has exited. Goodbye!"
sleep 5s
