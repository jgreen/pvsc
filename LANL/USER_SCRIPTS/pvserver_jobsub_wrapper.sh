#!/bin/bash
# ParaView Server job submission wrapper script. by G.Cone LANL HPC-3
#   Based on NERSC's PV server job submission wrapper script. Modified
#   for our systems.

usage(){
  cat<<EOF
    expected 6 arguments but got $#
    args:
    $*
   
    Usage: $0 NNODES NCPUS_PER_NODE WALLTIME IPORT CONID [PV_VER]
   
      NNODES           - number of compute nodes to use.
      NCPUS_PER_NODE   - number of processes per compute node.
      MINUTES          - wall time in MINUTES.
      ACCOUNT          - Moab account to use for job
      IPORT            - intermediate port tunnel via front-end node
      CONID            - connection ID for a client-server session
      PV_VER           - ParaView Version number
EOF
} #END function usage

# if not enough arguments, then epic fail
if [ $# -lt 6 ]; then
  usage
  exit 1
fi

NNODES=$1
NCPUS_PER_NODE=$2
MINUTES=$3
ACCOUNT=$4
IPORT=$5
CONID=$6
PV_VER=

# For the newer LANL PVSC files, a 7th argument will 
# specify the ParaView Version number
if [ $# -gt 6 ]; then
  PV_VER=$7
else
  PV_VER=4.3.1
fi

# include the following to allow this script to end gracefully if the client-side PV
# is unexpectedly terminated
trap "echo Cleaning up... && trap - SIGTERM && kill -- -$$ 1>/dev/null 2>/dev/null" SIGINT SIGTERM EXIT

TOT_TASKS=$((NNODES*NCPUS_PER_NODE))

WALLTIME=${MINUTES}:00
ACCT_ARG=

# if the ACCOUNT variable is NOT "DEFAULT", add in an msub argument to 
# specify the account to use
if [[ $ACCOUNT != "DEFAULT" ]]; then
  ACCT_ARG="-A $ACCOUNT"
fi

# Ensure the User is able to see the DOE "Notice to Users" banner.
cat /etc/motd
sleep 5

# cat the snazzy ParaView FIGlet banner on the little XTerm for user entertainment
cat /usr/projects/hpcsoft/USER_SCRIPTS/ParaView_figlet_iso1.txt
echo -------------------------------------------------------------------------------
echo "V E R S I O N                                                             ${PV_VER}"
echo -------------------------------------------------------------------------------

# Submit the PVserver job using an msub with a HereDoc:
echo Submitting pvserver job for $TOT_TASKS tasks across $NNODES nodes for ${WALLTIME}.

JOBID=$(msub $ACCT_ARG <<EOF
  #!/bin/tcsh
  #MSUB -l nodes=$NNODES:ppn=$NCPUS_PER_NODE
  #MSUB -l walltime="$WALLTIME"

  module purge
  module load sandbox
  module load friendly-testing
  module load paraview/${PV_VER}-osmesa

  # launch the pvserver processes:
  /usr/bin/srun -N $NNODES -n $TOT_TASKS --ntasks-per-node=$NCPUS_PER_NODE pvserver --use-offscreen-rendering \
  -rc -ch=localhost -sp=11111 --connect-id=$CONID --timeout=$MINUTES


#   /usr/bin/srun -N $NNODES -n $TOT_TASKS echo My name is `hostname`
#   sleep 120s
EOF
)
# Echo out JobID to ensure that the above is actually happening
echo Just submitted job $JOBID 

# Perform a do nothing while loop to periodically check every 5 seconds if the job is running:
echo Waiting for job to enter the RUNNING state. Please be patient if system is busy.
echo PLEASE DO NOT KILL THIS XTERM WINDOW!

while ! squeue -j $JOBID -o %T | grep RUNNING > /dev/null;do
  sleep 15s
done

echo pvserver allocation granted. Initating reverse connection between pvserver and your Paraview client.

# Now that the above loop is done, extract the lowest numbered node in the allocation and perform a backgrounded ssh to it:
SLURM_NODELIST=`squeue -j $JOBID -o %N | awk '$0 !~ /^NODELIST/'`

MASTER_NODE=`scontrol show hostnames "$SLURM_NODELIST" | head -n 1`

eval "ssh -N -T -R 11111:localhost:${IPORT} $MASTER_NODE &"
sleep 5s
#Save the PID of the backgrounded tunneling SSH process:
TUNNEL_PID=$! 

echo Your Paraview client should now be connected to the pvserver.
# Now do another while loop to ensure that the job is still running (check every minute).
while squeue -j $JOBID -o %T | grep RUNNING > /dev/null;do
  sleep 15s
done

# I'm finding that explicity killing the SSH tunnel is not needed as when the pvserver
# process ends, so does the job and losing the node results in the connection closing.
#echo "Killing SSH tunnel from ${HOSTNAME} to master compute node."
#kill -9 $TUNNEL_PID

echo Job has exited. Goodbye!
sleep 3s
