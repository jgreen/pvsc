#!/bin/bash
#PBS -S /bin/bash
#PBS -N ParaView
#PBS -o pvserver.out
#PBS -j oe

echo "---------- BEGIN PVSERVER LAUNCHER ----------"
date +"%F %T"
echo "---------------------------------------------"
echo "Sourcing bash init"
source /opt/modules/default/init/bash

module swap PrgEnv-${PE_ENV,,} PrgEnv-intel
module load craype-haswell
module load cray-hdf5

module use /usr/projects/hpcsoft/cle6.0/modulefiles/gadget/visualization
module load paraview/${PV_VERSION}-openswr-shared

echo -n "Getting MOM IP: "
MOM_IP=$(ip addr show dev $MOM_IF | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
echo ${MOM_IP}

# Keep generating a random port number until we find one not in use
echo -n "Generating MOM Port: "
MOM_PORT=$(($RANDOM+1024))
while netstat -ltnT | sed -n "s/^tcp \+[0-9]\+ \+[0-9]\+ \+\(0.0.0.0\|$MOM_IP\):\([0-9]\+\).*/\1 \2/p" | grep $MOM_PORT > /dev/null
do
  MOM_PORT=$(($RANDOM+1024))
done
echo ${MOM_PORT}

echo -n "Setting number of SWR threads: "
N_CPUS_PER_NODE=$(aprun -n 1 cat /proc/cpuinfo | awk '/^physical id/{printf("PID:%d",$4)} /^core id/{printf(" CID:%d\n",$4)}' | sort | uniq | wc -l)
N_CPUS_PER_PROC=$(($N_CPUS_PER_NODE/$PBS_NUM_PPN))
N_NUMA_PER_NODE=$(aprun -n 1 numactl -H | awk '/^available:/{print $2}')
N_NUMA_PER_PROC=$((($N_NUMA_PER_NODE+$PBS_NUM_PPN-1)/$PBS_NUM_PPN))
N_CPUS_PER_NUMA=$(($N_CPUS_PER_NODE/$N_NUMA_PER_NODE))
if [ $N_CPUS_PER_NUMA -gt $N_CPUS_PER_PROC ]
then
  N_CPUS_PER_NUMA=$N_CPUS_PER_PROC
fi
export KNOB_MAX_THREADS_PER_CORE=1
export KNOB_MAX_CORES_PER_NUMA_NODE=$N_CPUS_PER_NUMA
export KNOB_MAX_NUMA_NODES=$N_NUMA_PER_PROC
echo $(($KNOB_MAX_CORES_PER_NUMA_NODE*$KNOB_MAX_NUMA_NODES))

echo "Starting up the intermediate socat tunnel"
socat -d -d -lf socat-mom.log tcp4-listen:$MOM_PORT,bind=$MOM_IP,close tcp4-connect:$FE_IP:$FE_PORT &

# launch the pvserver processes:
aprun -B pvserver \
  --use-offscreen-rendering \
  -rc -ch=$MOM_IP -sp=$MOM_PORT \
  --timeout=$MINUTES
