#!/bin/bash
# ParaView Server job submission wrapper script. by G.Cone LANL HPC-3
#   Based on NERSC's PV server job submission wrapper script. Modified
#   for our systems.

usage(){
  cat<<EOF
    expected 6 or 7 arguments but got $#
    args:
    $*
   
    Usage: $0 NNODES NCPUS_PER_NODE WALLTIME IPORT MPORT [PV_VER]
   
      NNODES           - number of compute nodes to use.
      NCPUS_PER_NODE   - number of processes per compute node.
      MINUTES          - wall time in MINUTES.
      ACCOUNT          - Moab account to use for job
      IPORT            - intermediate port tunnel via front-end node
      CONID            - connection ID for a client-server session
      PV_VER           - ParaView Version Number of Client
EOF
} #END function usage

# if not enough arguments, then epic fail
if [ $# -lt 6 ]; then
  usage
  exit 1
fi

NNODES=$1
NCPUS_PER_NODE=$2
MINUTES=$3
ACCOUNT=$4
IPORT=$5
CONID=$6
PV_VER=
LOAD_FT=

# Use a random number between 1025 and 65355 for the MOM PORT
MPORT=$(( (RANDOM % 64511) + 1025 ))

if [ $# -gt 6 ]; then
  PV_VER=$7
else 
  PV_VER=4.3.1
fi

TOT_TASKS=$((NNODES*NCPUS_PER_NODE))

WALLTIME=${MINUTES}:00
ACCT_ARG=

# if the ACCOUNT variable is NOT "DEFAULT", add in an msub argument to 
# specify the account to use
if [[ $ACCOUNT != "DEFAULT" ]]; then
  ACCT_ARG="-A $ACCOUNT"
fi

# Ensure the User is able to see the DOE "Notice to Users" banner.
cat /etc/motd
sleep 5

# cat the snazzy ParaView FIGlet banner on the little XTerm for user entertainment
cat /usr/projects/hpcsoft/USER_SCRIPTS/ParaView_figlet_iso1.txt
echo -------------------------------------------------------------------------------
echo "V E R S I O N                                                             ${PV_VER}"
echo -------------------------------------------------------------------------------

# Submit the PVserver job using an msub with a HereDoc:
echo Submitting pvserver job for $TOT_TASKS tasks across $NNODES nodes for ${WALLTIME}.

# Network interface netween the mom node and the nid
MOM_IF=ipogif0

JOBID=$(qsub $ACCT_ARG -q ccm_queue <<EOF
#!/bin/tcsh
#PBS -S /bin/tcsh
#PBS -N whatev
#PBS -o ${HOME}/pvjob.out
#PBS -j oe
#PBS -l nodes=$NNODES:ppn=$NCPUS_PER_NODE
#PBS -l walltime="$WALLTIME"

echo "Sourcing tcsh init"
source /opt/modules/default/init/tcsh

# Get the IP addres of the mom node
echo "Getting MOM IP"
set MOM_IP=\`ip addr show dev $MOM_IF | sed -n 's|.*inet \([0-9\.]*\).*|\1|p'\`

module purge
module load modules
module load PrgEnv-gnu
module load cray-mpich
module load paraview/${PV_VER}-osmesa-static

# launch the pvserver processes:
aprun pvserver \
  --use-offscreen-rendering \
  -rc -ch=\$MOM_IP -sp=$MPORT \
  --connect-id=$CONID --timeout=$MINUTES
EOF
)
JOBID=$(echo $JOBID | grep -o "^[0-9]*")

# Perform a do nothing while loop to periodically check every 5 seconds if the job is running:
echo Waiting for job to enter the RUNNING state. Please be patient if system is busy.
echo PLEASE DO NOT KILL THIS XTERM WINDOW!

while ! checkjob $JOBID 2>&1 | grep "State: Running" > /dev/null;do
  sleep 15s
done

echo pvserver allocation granted. Initating reverse connection between pvserver and your Paraview client.

# Now that the above loop is done, extract the MOM node in the allocation and perform a backgrounded ssh to it:
MOM_NODE=$(qstat -f $JOBID | sed -n "s/ *login_node_id = \(.*\)/\1/p")

SCRIPT_DIR=$(dirname $(readlink -f $BASH_SOURCE) )

ssh -o StrictHostKeyChecking=no -tt -R $MPORT:localhost:${IPORT} $MOM_NODE \
  ${SCRIPT_DIR}/socat-fwd-if ${MOM_IF} ${MPORT} localhost $MPORT &

sleep 5s
#Save the PID of the backgrounded tunneling SSH process:
TUNNEL_PID=$! 

echo Your Paraview client should now be connected to the pvserver.

# Now do another while loop to ensure that the job is still running (check every 15s).
while checkjob $JOBID | grep "State: Running" > /dev/null;do
  sleep 15s
done

# I'm finding that explicity killing the SSH tunnel is not needed as when the pvserver
# process ends, so does the job and losing the node results in the connection closing.
#echo "Killing SSH tunnel from ${HOSTNAME} to master compute node."
#kill -9 $TUNNEL_PID

echo Job has exited. Goodbye!
sleep 3s
